const axios = require('axios'),
 { Client } = require('@elastic/elasticsearch'),
  client = new Client({ node: 'http://es01:9200' })


module.exports = (app) => {
	const list = async (req, res) => {
		let result = await axios.get('https://jsonplaceholder.typicode.com/users');

		let websites = result.data.map((item) => item.website);

		let objDate = new Date()
	
		// await client.index({
		// 	index: 'websites',
		// 	body: websites
		// })

		let users = result.data.map((item) => {
			return { name: item.name, email: item.email, company: item.company };
		}).sort(function(obj1, obj2) {
			return obj1.name < obj2.name ? -1 : obj1.name > obj2.name ? 1 : 0
        });

		// await client.index({
		// 	index: 'users',
		// 	body: users
		// })
        
        let suites = result.data.filter(item=>{
            return (item.address.suite.indexOf('Suite')>-1)
        }).map(item=>{
            return item.name
        })

		// await client.index({
		// 	index: 'suites',
		// 	body: suites
		// })

		res.send({ websites, users,suites });
	};

	return { list };
};
