const express = require('express'),
	consign = require('consign');

const PORT = 3000;
const HOST = '0.0.0.0';

const app = express();

consign().include('./services').then('./routes').into(app);

app.listen(PORT, HOST);
